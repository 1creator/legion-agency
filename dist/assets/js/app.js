"use strict";
(self["webpackChunkgulp"] = self["webpackChunkgulp"] || []).push([[143],{

/***/ 900:
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

/* unused harmony export default */
/* harmony import */ var bootstrap_js_src_modal_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(901);
/* harmony import */ var bootstrap_js_src_dropdown_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(671);
/* harmony import */ var bootstrap_js_src_collapse_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(29);
/* harmony import */ var bootstrap_js_src_button_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(815);
/* harmony import */ var bootstrap_js_src_tab_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(233);
/* harmony import */ var swiper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(257);







const formatter = new Intl.NumberFormat("ru-RU", {style: 'currency', currency: 'RUB'});

function formatCurrency(value) {
    return formatter.format(value);
}


swiper__WEBPACK_IMPORTED_MODULE_5__/* .Swiper.use */ .tq.use([swiper__WEBPACK_IMPORTED_MODULE_5__/* .Navigation */ .W_, swiper__WEBPACK_IMPORTED_MODULE_5__/* .Pagination */ .tl]);

// Слайдер с видео
new swiper__WEBPACK_IMPORTED_MODULE_5__/* .Swiper */ .tq(".blog__video-slider", {
    loop: true,
    slidesPerView: 1.1,
    spaceBetween: 24,
    navigation: {
        nextEl: ".video-slider__button-next",
        prevEl: ".video-slider__button-prev",
    },
    breakpoints: {
        960: {
            slidesPerView: 3,
        },
    },
});

// Слайдер со статьями
new swiper__WEBPACK_IMPORTED_MODULE_5__/* .Swiper */ .tq(".blog__article-slider", {
    loop: true,
    slidesPerView: 1.1,
    spaceBetween: 24,
    navigation: {
        nextEl: ".article-slider__button-next",
        prevEl: ".article-slider__button-prev",
    },
    breakpoints: {
        960: {
            slidesPerView: 3,
        },
    },
});

// Настройка карты и переключения вкладок на карте
const offices = [
    {
        coords: [55.745615, 37.66733],
        name: "Москва",
        placemark: null,
    },
    {
        coords: [55.745615, 39.66733],
        name: "Санкт-Петербург",
        placemark: null,
    },
];
ymaps.ready(function () {
    const myMap = new ymaps.Map("ya-map", {
        center: [55.745732, 37.671777],
        zoom: 17,
        controls: [],
    });

    offices.forEach((i) => {
        const placemark = new ymaps.Placemark(
            i.coords,
            {
                hintContent: i.name,
            },
            {
                iconLayout: "default#image",
                iconImageHref: "assets/images/marker.svg",
                iconImageSize: [74, 106],
            }
        );
        i.placemark = placemark;
        myMap.geoObjects.add(placemark);
    });
});
const mapTabs = document.querySelectorAll(".map [data-bs-toggle]");
for (const tab of mapTabs) {
    tab.addEventListener("shown.bs.tab", function (event) {
        const placemarkName = event.target.dataset.id;
        if (!placemarkName) {
            console.error("Необходимо указать атрибут data-id");
            return;
        }
        const office = offices.find((i) => i.name === placemarkName);

        if (!office) {
            console.error("Не найден офис с указанным data-id");
            return;
        }

        office.placemark.getMap().setCenter(office.placemark.geometry.getCoordinates(), 12);
    });
}

// Валидация форм
const forms = document.querySelectorAll(".needs-validation");
Array.prototype.slice.call(forms).forEach(function (form) {
    form.addEventListener(
        "submit",
        function (event) {
            event.preventDefault();
            event.stopPropagation();
            if (form.checkValidity()) {
                const inputs = Array.prototype.slice.call(form.querySelectorAll("input"))
                const params = inputs.reduce((acc, i) => {
                    acc[i.name] = i.value
                    return acc
                }, {})
                console.log(params)
                form.classList.add("was-sent");
            }

            form.classList.add("was-validated");
        },
        false
    );
});

// Калькулятор
const calculatorEl = document.querySelector(".calculator")
if (calculatorEl) {
    const submitBtn = calculatorEl.querySelector('.calculator__submit');
    const countEl = calculatorEl.querySelector(".calculator__count")
    const prepaymentEl = calculatorEl.querySelector(".calculator__prepayment")
    const avgEl = calculatorEl.querySelector(".calculator__avg")
    const checkboxes = Array.prototype.slice.call(calculatorEl.querySelectorAll("[data-avg]"))
    const collapses = Array.prototype.slice.call(calculatorEl.querySelectorAll(".collapse"))
        .map(el => {
            return new bootstrap_js_src_collapse_js__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z(el, {toggle: false})
        })
    checkboxes.forEach(function (cb) {
        cb.addEventListener("input", () => {
            const checkedInputs = checkboxes.filter(i => i.checked)

            if (checkedInputs.length) {
                const avg = checkedInputs.reduce((acc, i) => acc + +i.dataset.avg, 0)
                const prepayment = checkedInputs.reduce((acc, i) => acc + +i.dataset.prepayment, 0)
                countEl.innerHTML = String(checkedInputs.length)
                avgEl.innerHTML = String(formatCurrency(avg));
                prepaymentEl.innerHTML = String(formatCurrency(prepayment));
                collapses.forEach(i => i.show())

                const names = checkedInputs.map(i => i.dataset.name).join(", ");
                submitBtn.setAttribute('data-uf-settings', JSON.stringify({
                    "formConfig": "3_consult",
                    "fields": {
                        "avg": avg,
                        "prepayment": prepayment,
                        "services": names,
                    }
                }));
            } else {
                collapses.forEach(i => i.hide())
                submitBtn.setAttribute('data-uf-settings', JSON.stringify({
                    "formConfig": "3_consult"
                }));
            }
        })
    })
}

const serviceList = document.querySelectorAll('.services-list__content');
for (let list in serviceList) {
    const tabs = document.querySelectorAll('[data-bs-parent="#private-services"]')
    for (const item of tabs) {
        item.addEventListener('shown.bs.collapse', function (el) {
            list.style.minHeight = `${el.target.offsetHeight}px`;
        })
    }
}


/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, [736], () => (__webpack_exec__(900)));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);