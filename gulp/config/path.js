import * as nodePath from "path";

const rootFolder = nodePath.basename(nodePath.resolve());
const buildFolder = "./dist";
const srcFolder = "./src";

export const path = {
    build: {
        js: `${buildFolder}/assets/js/`,
        css: `${buildFolder}/assets/css/`,
        html: `${buildFolder}/`,
        fonts: `${buildFolder}/assets/fonts/`,
        assets: `${buildFolder}/assets/images/`,
    },
    src: {
        js: [
            `${srcFolder}/assets/js/app.js`,
        ],
        scss: `${srcFolder}/assets/scss/*.scss`,
        html: `${srcFolder}/*.html`,
        assets: `${srcFolder}/assets/images/**/*.*`,
        fonts: `${srcFolder}/assets/scss/vendor/icons/fonts/*.*`,
    },
    watch: {
        js: `${srcFolder}/assets/js/*.js`,
        scss: [
            `${srcFolder}/assets/scss/**/*.scss`,
            `!${srcFolder}/assets/scss/vendor.scss`,
        ],
        appScss: `${srcFolder}/assets/scss/app.scss`,
        html: `${srcFolder}/**/*.html`,
        assets: `${srcFolder}/assets/images/**/*.*`,
        fonts: `${srcFolder}/assets/scss/vendor/icons/fonts/*.*`,
    },
    clean: buildFolder,
    buildFolder,
    srcFolder,
    rootFolder,
};
